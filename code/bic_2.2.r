library(tidyverse);

## p1
input <- readLines("../input/2.2-p1.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.2-p1.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  input[(linePos + 1):(linePos + params[1])] %>%
    strsplit("") %>%
    sapply(as.numeric) -> seqs;
  apply(seqs,1,paste,collapse="") %>%
    factor(., levels=unique(.)) -> methSeqs;
  cat(length(levels(methSeqs)), "\n",
      file="../output/2.2-p1.txt", sep="", append=TRUE);
  cat(length(levels(methSeqs)), "\n", sep="");
  cat(paste(as.numeric(methSeqs), collapse=" "), "\n",
      file="../output/2.2-p1.txt", sep="", append=TRUE);
  cat(paste(head(as.numeric(methSeqs)), collapse=" "), "\n");
  linePos <- linePos + 1 + params[1];
}

## p2
input <- readLines("../input/2.2-p2.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.2-p2.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  input[(linePos + 1):(linePos + params[1])] %>%
    strsplit("") %>%
    sapply(as.numeric) -> seqs;
  apply(seqs,1,paste,collapse="") %>%
    factor(., levels=unique(.)) -> methSeqs;
  cat(length(levels(methSeqs)), "\n",
      file="../output/2.2-p2.txt", sep="", append=TRUE);
  cat(length(levels(methSeqs)), "\n", sep="");
  cat(paste(as.numeric(methSeqs), collapse=" "), "\n",
      file="../output/2.2-p2.txt", sep="", append=TRUE);
  cat(paste(head(as.numeric(methSeqs)), collapse=" "), "\n");
  linePos <- linePos + 1 + params[1];
}

