library(tidyverse);

inFileName <- "../input/3.3/07";
outFileName <- "../output/3.3-p6.txt";
cat("",file=outFileName, append=FALSE);

## NOTE: test limits!

lineSkip <- 0;
numTests <- scan(inFileName, n = 1);
lineSkip <- 1;
problem <- 0;
numRemainingTests <- numTests;

params <- scan(inFileName, n=2, skip=lineSkip);
nOrgs <- params[1];
nBlocks <- params[2];
lineSkip <- lineSkip + 1;
cases <- head(tail(readLines(inFileName), -lineSkip), nOrgs * 3);
hasDisease <- cases[seq(1, nOrgs * 3, by=3)] == "+";
lineSkip <- lineSkip + nOrgs * 3;
problem <- problem + 1;
blocks1 <- cases[seq(2, nOrgs * 3, by=3)] %>%
  sapply(function(x){c("A"=0,"C"=1,"G"=2,"T"=3)[unlist(strsplit(x, ""))]});
blocks2 <- cases[seq(3, nOrgs * 3, by=3)] %>%
  sapply(function(x){c("A"=0,"C"=1,"G"=2,"T"=3)[unlist(strsplit(x, ""))]});
colnames(blocks1) <- NULL;
colnames(blocks2) <- NULL;

blockWidth <- 4;
candPoss <- 400:501;
candPoss <- (min(candPoss)+blockWidth+1):(max(candPoss)-blockWidth);

controls <- rep(0, length(candPoss));
uniqueness <- rep(0, length(candPoss));
for(x in seq_along(candPoss)){
  candPos <- candPoss[x];
  blocks1[(candPos-blockWidth):(candPos+blockWidth),hasDisease, drop=FALSE] %>%
    apply(2,paste, collapse="") %>%
    unique() -> ub1;
  blocks2[(candPos-blockWidth):(candPos+blockWidth),hasDisease, drop=FALSE] %>%
    apply(2,paste, collapse="") %>%
    unique() -> ub2;
  c(ub1, ub2) %>% unique() %>% length() -> uniqueness[x];
  blocks1[(candPos-blockWidth):(candPos+blockWidth),!hasDisease, drop=FALSE] %>%
    apply(2,paste, collapse="") %>%
    unique() -> cb1;
  blocks2[(candPos-blockWidth):(candPos+blockWidth),!hasDisease, drop=FALSE] %>%
    apply(2,paste, collapse="") %>%
    unique() -> cb2;
  c(cb1, cb2) %>% unique() %>% length() -> controls[x];
}
# 400 501 12.5
# 400-420 16.67

plot(candPoss-1, uniqueness, pch=21, bg="#0000FF80", 
     ylim=c(min(uniqueness, controls),
            max(uniqueness, controls)), main=sprintf("problem %d", problem))
points(candPoss-1, controls, pch=21, bg="#FF000080", col=NA)
