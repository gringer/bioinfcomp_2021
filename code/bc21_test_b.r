#!/usr/bin/env Rscript
library(tidyverse);

data.in <- readLines("input.txt")[-1];

data.strings <- data.in[seq(1, 20, by=2)];
data.search <- data.in[seq(2, 20, by=2)];

cat("", file="output-B.txt");
for(pos in seq_along(data.strings)){
  pos.search <- data.search[pos];
  pos.string <- data.strings[pos];
  poss <- which(pos.search == substring(pos.string,
                                  first = 1:(nchar(pos.string) - nchar(pos.search)),
                                  last = 1:(nchar(pos.string) - nchar(pos.search)) + 
                                    nchar(pos.search) - 1));
  cat(poss, sep=" ", file="output-B.txt", append=TRUE);
  cat("\n", file="output-B.txt", append=TRUE);
}