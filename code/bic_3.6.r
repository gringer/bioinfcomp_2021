library(tidyverse);

inFileName <- "../input/3.6/30-mouse-exact.txt";
outFileName <- "../output/3.6-p3.txt";

params <- scan(inFileName, n = 2);
niso <- params[1];
delta <- params[2];

head(readLines(inFileName)[-1], niso) %>%
  strsplit("[,-]") %>%
  sapply(function(x){matrix(as.numeric(x), nrow = 2)}) -> isoforms;

tail(readLines(inFileName), -(niso+2)) %>%
  strsplit("[,-]") %>%
  sapply(function(x){matrix(as.numeric(x), nrow = 2)}) -> reads;

res <- rep("", length(reads));

tibble(type="isoform", 
       id=as.character(rep(1:niso,sapply(isoforms, ncol))-1),
       start=unlist(sapply(isoforms, function(x){x[1,]})),
       end=unlist(sapply(isoforms, function(x){x[2,]}))) %>%
  arrange(start) -> isos.tbl;


tibble(type="read",
       id=as.character(rep(1:length(reads),sapply(reads, ncol))-1),
       start=unlist(sapply(reads, function(x){x[1,]})),
       end=unlist(sapply(reads, function(x){x[2,]}))) %>%
  arrange(start) -> reads.tbl;

res <- rep("", length(unique(reads.tbl$id)));

for(rid in sort(as.numeric(unique(reads.tbl$id)))){
  if(rid %% 100 == 0){
    cat(rid, "\n");
  }
  reads.tbl %>%
    filter(id == rid) -> subRead.tbl;
  readStart <- min(subRead.tbl$start);
  firstEnd <- min(subRead.tbl$end);
  readEnd <- max(subRead.tbl$end);
  lastStart <- max(subRead.tbl$start);
  isos.tbl %>% 
    filter(end >= readStart, start <= readEnd) %>%
    filter((start > firstEnd) | (start <= readStart),
           (end < lastStart) | (end >= readEnd)) -> subIso.tbl
  subIso.tbl %>%
    filter(start <= readStart, end == firstEnd) %>%
    pull(id) -> startMatch.ids;
  subIso.tbl %>%
    filter(end >= readEnd, start == lastStart) %>%
    pull(id) -> endMatch.ids;
  trimmedLengths <- subIso.tbl %>%
    group_by(id) %>% summarise(exonCount=n()) %>%
    filter(exonCount == nrow(subRead.tbl)) %>%
    pull(id) -> lengthMatch.ids;
  candidate.ids <- intersect(intersect(startMatch.ids, endMatch.ids), 
                             lengthMatch.ids);
  validIDs <- rep(TRUE, length(candidate.ids));
  names(validIDs) <- candidate.ids;
  for(idn in candidate.ids){
    subFilt.tbl <- subIso.tbl %>% filter(id == idn);
    if(!all((subRead.tbl %>% pull(start) %>% tail(-1)) %in%
            (subFilt.tbl %>% pull(start)))){
      validIDs[idn] <- FALSE;
    }
    if(!all((subRead.tbl %>% pull(end) %>% head(-1)) %in%
            (subFilt.tbl %>% pull(end)))){
      validIDs[idn] <- FALSE;
    }
    if(!all((subFilt.tbl %>% pull(start) %>% tail(-1)) %in%
            (subRead.tbl %>% pull(start)))){
      validIDs[idn] <- FALSE;
    }
    if(!all((subFilt.tbl %>% pull(end) %>% head(-1)) %in%
            (subRead.tbl %>% pull(end)))){
      validIDs[idn] <- FALSE;
    }
  }
  if(sum(validIDs) >= 1){
    res[rid+1] <- sprintf("%d %d",
                min(as.numeric(names(which(validIDs)))), 
                sum(validIDs));
  } else {
    #cat("No solution for", rid, "\n");
    res[rid+1] <- "-1 0";
  }
}

cat(res, file=outFileName, sep="\n");
