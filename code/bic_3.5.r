library(Biostrings);
library(tidyverse);

inFileName <- "../input/3.5/in1.txt";
outFileName <- "../output/3.5-p1.txt";
cat("",file=outFileName, append=FALSE);

params <- scan(inFileName, n = 3);
numTests <- params[1];
scoreMax <- params[2];
beta <- params[3];
scoreExtra <- scan(inFileName, skip=2, what=character(), n=1) == "+";
lineSkip <- 3;

scan(inFileName, skip=lineSkip, what=character(), n=2)[-1] -> subRef;
scan(inFileName, skip=lineSkip+2, n=4) -> subParams;
minPoly <- subParams[1]; numReads <- subParams[2];
hapRate <- subParams[3]; errRate <- subParams[4];
scan(inFileName, what=character(), skip=lineSkip+4, n=numReads) -> seqReads;

allReads <- DNAStringSet(c(subRef, seqReads)); 

dMat <- as.matrix(stringDist(allReads, method = "hamming"))
readDiffs <- apply(dMat,1,median, na.rm=TRUE) >= minPoly;

sum(readDiffs) / numReads;

cat(rep(1, numTests), file=outFileName, sep="\n");
