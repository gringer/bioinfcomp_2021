inFileName <- "../input/3.3/04";
outFileName <- "../output/3.3-p2.txt";
cat("",file=outFileName, append=FALSE);

## NOTE: test limits!

lineSkip <- 0;
numTests <- scan(inFileName, n = 1);
lineSkip <- 1;
numRemainingTests <- numTests;

params <- scan(inFileName, n=2, skip=lineSkip);
nOrgs <- params[1];
nBlocks <- params[2];
lineSkip <- lineSkip + 1;
cases <- head(tail(readLines(inFileName), -lineSkip), nOrgs * 2);
hasDisease <- cases[seq(1, nOrgs * 2, by=2)] == "+";
lineSkip <- lineSkip + nOrgs * 2;

blocks <- cases[seq(2, nOrgs * 2, by=2)] %>%
  sapply(function(x){c("A"=0,"C"=1,"G"=2,"T"=3)[unlist(strsplit(x, ""))]});
colnames(blocks) <- NULL;

blockWidth <- 3;
candPoss <- (1+blockWidth):(nBlocks-blockWidth);

candPoss <- 450:500;

controls <- rep(0, length(candPoss));
uniqueness <- rep(0, length(candPoss));
for(x in seq_along(candPoss)){
  candPos <- candPoss[x];
  blocks[(candPos-blockWidth):(candPos+blockWidth),hasDisease] %>%
    apply(2,paste, collapse="") %>%
    unique() %>% length() -> uniqueness[x];
  blocks[(candPos-blockWidth):(candPos+blockWidth),!hasDisease] %>%
    apply(2,paste, collapse="") %>%
    unique() %>% length() -> controls[x];
}
plot(candPoss, uniqueness, 
     ylim=c(min(uniqueness, controls), max(uniqueness, controls)))
points(candPoss, controls, pch=21, bg="#00000030", col=NA)

plot(which(apply(blocks[,hasDisease], 1, function(x){length(unique(x))}) == 2))

plot(which(apply(blocks[,!hasDisease], 1, function(x){length(unique(x))}) == 2))
