library(Biostrings);
library(tidyverse);

inFileName <- "../input/3.2/test1.txt";
outFileName <- "../output/3.2-p1.txt";
cat("",file=outFileName, append=FALSE);

## load in data
params <- scan(inFileName, n=2);
numPeople <- params[1];
numGTs <- params[2];
phasedPeople <- tail(readLines(inFileName, n = numPeople*3+1), -1);
phasedPeople <- phasedPeople[-seq(1,numPeople*3,by=3)] %>%
  strsplit("") %>% sapply(as.numeric)
gts <- tail(readLines(inFileName, n = numPeople*3+numGTs*2+1), 
             numGTs*2)[seq(2,numGTs*2,by=2)] %>% gsub("\\?", ".", .);

possibleGTs <- outer(1:(numPeople*2), 1:(numPeople*2), function(X, Y){
  sapply(1:length(X), function(xi){
    paste(phasedPeople[,X[xi]] + phasedPeople[,Y[xi]], collapse="");
  })
}) %>% c();

possibleGTs %>%
  strsplit("") %>%
  sapply(as.numeric) -> gtArray

apply(gtArray, 1, function(x){-table(x) %>% sort() %>% head(1) %>% names}) -> bestGT;

resGTs <- gts;

span <- min(6, floor(ncol(phasedPeople)/3));
for(xi in seq_along(gts)){
  cat(xi,"\n");
  gtx <- gts[xi];
  if(any(grepl(gtx, possibleGTs))){
    grep(gtx, possibleGTs, value=TRUE) %>% 
      table() %>% which.max() %>% names();
  } else {
    for(stride in 0:(nchar(gtx) - span)){
      searchStr <- substring(gtx, 1+stride, span+stride);
      subGTs <- substring(possibleGTs, 1+stride, span+stride) %>% c();
      #cat(sprintf("Searching for '%s' at stride %d... ", searchStr, stride));
      if(grepl("\\.", searchStr) && any(grepl(searchStr, unique(subGTs)))){
        #cat(sprintf(" found\n", stride));
        substring(gtx, 1+stride, span+stride) <-
          grep(searchStr, unique(subGTs), value=TRUE) %>% 
          table() %>% which.max() %>% names();
      } else if(grepl(".", searchStr, fixed=TRUE)) {
        ## replace dots with most frequent genotype [Note: not allowed to change set GTs]
        changePoss <- gregexpr(".", searchStr, fixed=TRUE)[[1]];
        #cat(sprintf("replacing '%s' from %d to %d (stride: %d; span:%d)\n", 
        #            searchStr, 1+stride, span+stride, stride, span));
        for(cp in changePoss){
          #cat(sprintf("changing %d from '%s' to '%s'\n", cp,
          #            substring(searchStr, cp, cp),
          #            bestGT[cp+stride]));
          substring(searchStr, cp, cp) <- bestGT[cp+stride];
        }
        #cat(sprintf("replStr is now '%s'\n", searchStr));
        substring(gtx, 1+stride, span+stride) <- searchStr;
        #cat(sprintf("gtx is now '%s'\n", substring(gtx, 1+stride, span+stride)));
      } else {
        #cat(" nothing to do!\n");
      }
    }
  }
  resGTs[xi] <- gtx;
}

cat(resGTs, file=outFileName, sep="\n");
