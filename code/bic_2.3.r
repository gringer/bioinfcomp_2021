library(tidyverse);

## p1
input <- readLines("../input/2.3-p1.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.3-p1.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  Mn <- params[1]; An <- params[2]; Sn <- params[3];
  input[linePos+1] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> M;
  input[linePos+2] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> A;
  input[linePos+3] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> S;
  ## Exhaustive approach
  MAsums <- outer(M, A, "+");
  for(s in S){
    cat(s, ":");
    newSums <- abs(MAsums - s);
    minPos <- head(which(newSums == min(newSums), arr.ind = TRUE), 1);
    cat(minPos,"\n");
    cat(paste(minPos, collapse=" "), "\n", file="../output/2.3-p1.txt", sep="", append=TRUE);
  }
  linePos <- linePos + 3;
}

## p2
input <- readLines("../input/2.3-p2.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.3-p2.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  Mn <- params[1]; An <- params[2]; Sn <- params[3];
  input[linePos+1] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> M;
  input[linePos+2] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> A;
  input[linePos+3] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> S;
  ## Exhaustive approach
  MAsums <- outer(M, A, "+");
  if(length(S) != Sn){
    stop("incorrect number of signals");
  }
  for(s in S){
    newSums <- abs(MAsums - s);
    minPos <- head(which(newSums == min(newSums), arr.ind = TRUE), 1);
    cat(paste(minPos, collapse=" "), "\n", file="../output/2.3-p2.txt", sep="", append=TRUE);
  }
  linePos <- linePos + 4;
}

## p3
input <- readLines("../input/2.3-p3.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.3-p3.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  Mn <- params[1]; An <- params[2]; Sn <- params[3];
  input[linePos+1] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> M;
  input[linePos+2] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> A;
  input[linePos+3] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> S;
  ## Exhaustive approach
  MAsums <- outer(M, A, "+");
  if(length(S) != Sn){
    stop("incorrect number of signals");
  }
  for(s in S){
    newSums <- abs(MAsums - s);
    minPos <- head(which(newSums == min(newSums), arr.ind = TRUE), 1);
    cat(paste(minPos, collapse=" "), "\n", file="../output/2.3-p3.txt", sep="", append=TRUE);
  }
  linePos <- linePos + 4;
}

## p4/alt
input <- readLines("../input/2.3-p4.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.3-p4.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  Mn <- params[1]; An <- params[2]; Sn <- params[3];
  input[linePos+1] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> M;
  input[linePos+2] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> A;
  input[linePos+3] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> S;
  if(length(S) != Sn){
    stop("incorrect number of signals");
  }
  ## Determine difference, merge sort with M array, then work out smallest delta
  SAmat <- outer(S, A, "-"); ## fill M with A constant, then next A
  tibble(mergeVal=c(SAmat, M), SApos=1:(length(SAmat)+length(M))) %>%
    mutate(Spos = ifelse(SApos > length(SAmat), NA, (SApos-1) %% length(S) + 1),
           Apos = ((SApos-Spos) / length(S)) + 1,
           Mpos = ifelse(SApos > length(SAmat), SApos - length(SAmat), NA)) %>%
    arrange(mergeVal) -> merged.tbl  
  for(sp in 1:Sn){
    cat(sp, "\n");
    merged.tbl %>%
      filter(Spos == sp | !is.na(Mpos)) %>%
      mutate(pre.Mpos = lag(Mpos),
             post.Mpos = lead(Mpos)) %>%
      filter(!is.na(Spos), !is.na(Apos)) %>%
      filter(!is.na(pre.Mpos) | !is.na(post.Mpos)) %>%
      select(-Mpos) %>%
      pivot_longer(matches("(pre|post)"), values_to="Mpos",
                   names_to = c("dir", "stat"), names_pattern = "(.*?)\\.(.*)") %>%
      select(-stat) %>%
      filter(!is.na(Mpos)) %>%
      mutate(MAval = M[Mpos] + A[Apos], Sval = S[Spos],
             MASdelta = abs(MAval - Sval)) %>%
      slice_min(MASdelta, n=1, with_ties=FALSE) -> S.val.tbl
    cat(sprintf("%s %s\n", S.val.tbl$Mpos, S.val.tbl$Apos), 
      file="../output/2.3-p4.txt", sep="", append=TRUE);
  }
  linePos <- linePos + 4;
}

## p5
input <- readLines("../input/2.3-p5.txt");
numProbs <- as.numeric(input[1]);
linePos <- 2;
cat("", file="../output/2.3-p5.txt", append = FALSE);
for(p in 1:numProbs){
  cat(sprintf("reading problem %d at line position %d\n", p, linePos));
  input[linePos] %>%
    strsplit(" ") %>%
    unlist() %>%
    as.numeric() -> params;
  Mn <- params[1]; An <- params[2]; Sn <- params[3];
  input[linePos+1] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> M;
  input[linePos+2] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> A;
  input[linePos+3] %>%
    strsplit(" ") %>% unlist() %>% sub("\\.","",.) %>% as.numeric() -> S;
  if(length(S) != Sn){
    stop("incorrect number of signals");
  }
  ## Determine sum, merge sort with S array, then work out smallest delta
  MAmat <- outer(M, A, "+"); ## fill M with A constant, then next A
  merged <- c(S, MAmat);
  mergeOrder <- order(merged);
  invOrder <- 1:length(mergeOrder);
  invOrder[mergeOrder] <- invOrder; ## could also be done via `rank`
  tibble(pos = 1:length(S), Srank = invOrder[pos]) %>%
    arrange(Srank) %>%
    mutate(pre.Mrank = Srank-1, post.Mrank = Srank+1) -> S.tbl;
  ## note: there are two situations where the prior-ranked value is also in S
  ## so adjust those numbers
  S.tbl$pre.Mrank[S.tbl$pre.Mrank %in% S.tbl$Srank] <-
    S.tbl$pre.Mrank[S.tbl$pre.Mrank %in% S.tbl$Srank] - 1;
  S.tbl$post.Mrank[S.tbl$post.Mrank %in% S.tbl$Srank] <-
    S.tbl$post.Mrank[S.tbl$post.Mrank %in% S.tbl$Srank] + 1;
  S.tbl %>%
    pivot_longer(matches("(pre|post)"), values_to="Mrank",
                 names_to = c("dir", "stat"), names_pattern = "(.*?)\\.(.*)") %>%
    select(-stat) %>%
    mutate(Spos = mergeOrder[Srank]) %>%
    mutate(MApos = mergeOrder[Mrank] - length(S)) %>%
    mutate(Mpos = (MApos-1) %% length(M) + 1,
           Apos = ((MApos-Mpos) / length(M)) + 1,
           Mval = M[Mpos], Aval = A[Apos],
           MAval = Mval + Aval,
           MAvalActual = MAmat[MApos],
           Sval = S[Spos],
           delta = abs(MAval - Sval)) %>%
    group_by(pos) %>% 
    arrange(delta) %>%
    slice_head(n=1) -> S.val.tbl
  cat(sprintf("%s %s\n", S.val.tbl$Mpos, S.val.tbl$Apos), 
      file="../output/2.3-p5.txt", sep="", append=TRUE);
  linePos <- linePos + 4;
}
