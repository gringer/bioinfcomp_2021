library(tidyverse);
library(magrittr);
library(Matrix);

getNodeHeight <- function(vertex, verbosity=0){
  currentHeight <- rep(0, length(vertex));
  currentVertex <- rep(vertex);
  found <- ifelse(currentVertex == 1, TRUE, FALSE);
  it <- 0;
  while(!all(found) && (it < 20)){
    if(verbosity >= 1){
      cat(sprintf("iteration: %d\n", it));
    }
    currentHeight[!found] <-
      currentHeight[!found] +
      apply(nodeJumps[currentVertex[!found],,drop=FALSE],
            1,function(x){2^(sum(!is.na(x))-1)});
    currentVertex[!found] <-
      apply(nodeJumps[currentVertex[!found],,drop=FALSE],
            1,function(x){x[sum(!is.na(x))]});
    found <- ifelse((currentVertex == 1) |
                      (is.na(currentVertex)), TRUE, FALSE);
    if(verbosity >= 2){
      print(currentHeight);
      print(currentVertex);
      print(found);
    }
    it <- it + 1;
  }
  return(currentHeight);
}

inFileName <- "../input/2.4-p6.txt";
outFileName <- "../output/2.4-p6.txt";
cat("",file=outFileName, append=FALSE);

## load in data
nVerts <- scan(inFileName, n = 1);
content <- scan(inFileName, skip = 2, n = nVerts);
parents <- c(NA, scan(inFileName, skip = 1, n = nVerts-1));
table(table(diff(parents)))
cat("loading diseases...\n");
nDiseases <- scan(inFileName, skip = 3, n = 1);
diseaseLines <- readLines(inFileName, n=nDiseases+4) %>% tail(-4);
diseaseCounts <- as.numeric(sub(" .*$", "", diseaseLines));
diseaseData <- sub("^.*? ", "", diseaseLines) %>% scan(text=.);
disease.tbl <- tibble(disease=rep(1:nDiseases, diseaseCounts),
                      vert=diseaseData);
cat("loading patients...\n");
nPatients <- scan(inFileName, skip = 4+nDiseases, n = 1);
patientLines <- readLines(inFileName, n=nPatients+5+nDiseases) %>%
  tail(-(5+nDiseases));
patientCounts <- as.numeric(sub(" .*$", "", patientLines));
patientData <- sub("^.*? ", "", patientLines) %>% scan(text=.);
patient.tbl <- tibble(patient=rep(1:nPatients, patientCounts),
                      vert=patientData);
cat("done\n");

## Create jump array [https://www.youtube.com/watch?v=F-_1sbnPbWQ]
nodeJumps <- matrix(NA, nrow = nVerts, ncol=ceiling(log2(nVerts)));
nodeJumps[,1] <- parents;
for(jump in 2:ncol(nodeJumps)){
  nodeJumps[,jump] <- (nodeJumps[,jump-1])[nodeJumps[,jump-1]];
}

dropNodes <- function(vertexes, dropHeight){
  drop.bin <- as.integer(intToBits(dropHeight));
  for(dp in rev(which(drop.bin == 1))){
    vertexes <- nodeJumps[vertexes, dp];
  }
  return(vertexes);
}

nodeHeights <- getNodeHeight(1:nVerts, verbosity=1);

modPatient.tbl <- patient.tbl %>% mutate(disease=NA);
modDisease.tbl <- disease.tbl %>% arrange(-content[disease.tbl$vert]);
currentVxs <- 1:nVerts;
currentVxs[!(currentVxs %in% c(patient.tbl$vert, disease.tbl$vert))] <- NA;
currentNhs <- nodeHeights[currentVxs];
heightSteps <- diff(sort(unique(currentNhs), decreasing = TRUE));

for(sp in seq_along(heightSteps)){
  
  modPatient.tbl$vert %in% modDisease.tbl$vert
  nodesToDrop <-(nodeHeights[currentVxs] == max(nodeHeights[currentVxs]));
  dropHeight <- -heightSteps[sp];
  currentVxs[nodesToDrop] <- 
    dropNodes(currentVxs[nodesToDrop], dropHeight);
}

match.scores <- matrix(0, nrow = nPatients, ncol = nDiseases);

for(pp in 1:nPatients){
  for(dp in 1:nDiseases){
    cat(pp, dp, "\n");
    patient.tbl %>% filter(patient == 1) %>% pull(vert) -> p1;
    disease.tbl %>% filter(disease == 1) %>% pull(vert) -> d1;
    fV <- NULL;
    while(length(p1) > 1){
      while(!any(d1 %in% p1)){
        #print(c(max(content[d1]), max(content[p1])));
        d1[content[d1] >= max(content[p1])] <-
          parents[d1[content[d1] > max(content[p1])]]
        p1[content[p1] > max(content[d1])] <-
          parents[p1[content[p1] > max(content[d1])]]
      }
      fV <- c(fV, p1[p1 %in% d1]);
      p1 <- p1[!(p1 %in% d1)];
    }
    match.scores[pp, dp] <- sum(content[fV]);
  }
}
    
  
spineEnd <- 5000;

## fill out disease vertex table (excluding central spine)
nrowStart <- 1;
while(nrowStart != nrow(disease.tbl)){
    nrowStart <- nrow(disease.tbl);
    print(nrowStart);
    disease.tbl %<>%
        bind_rows(disease.tbl %>%
                    filter(nodeHeights[vert] > spineEnd) %>%
                    mutate(vert=parents[vert])) %>%
        filter(!is.na(vert)) %>%
        distinct(disease, vert, .keep_all = TRUE)
}

patient.tbl$found <- (patient.tbl$vert %in% disease.tbl$vert) |
  (patient.tbl$vert <= spineEnd);

## promote patient vertexes until they match disease vertexes (or sit in the spine)
while(!all(patient.tbl$found)){
  vertsToChange <- which(!(patient.tbl$found));
  cat(sprintf("Vertexes not yet discovered: %d\n", length(vertsToChange)));
  patient.tbl$vert[vertsToChange] <- 
    parents[patient.tbl$vert[vertsToChange]];
  patient.tbl$found <- (patient.tbl$vert %in% disease.tbl$vert) |
    (patient.tbl$vert <= spineEnd);
}

## fill in diseases at specific nodes
diseaseMat <- sparseMatrix(i = disease.tbl$vert, j=disease.tbl$disease,
                           x = content[disease.tbl$vert]);

## remove spine counts
diseaseMat[1:spineEnd,] <- 0;

getSpineDiseases <- function(vertex){
  disease.tbl %>% 
    filter(vert >= vertex, vert <= spineEnd) %>%
    pull(disease);
}

findDisease <- function(patientNum){
  ## non-spine sums
  cs <- colSums(diseaseMat[patient.tbl %>% 
                             filter(patient == patientNum, vert > spineEnd) %>% 
                             pull(vert),,drop=FALSE]);
  ## spine sums
  spinePoss <- patient.tbl %>% 
    filter(patient == patientNum, vert <= spineEnd) %>%
    pull(vert);
  for(sp in spinePoss){
    sd <- getSpineDiseases(sp);
    cs[sd] <- cs[sd] + content[sp];
  }
  which(cs == max(cs)) %>% head(1);
}

## p1/p4 (low numbers of patients)
patientRess <- sapply(1:nPatients, findDisease);

# staggered by 10 (e.g. p4)
patientRess <- rep(1, nPatients);
bracketStart <- seq(1, nPatients, by=10);

for(s in bracketStart){
  cat(s, "\n");
  patientRess[s:(s+9)] <- sapply(s:(s+9), findDisease);
}


# staggered (e.g. p3, p7)
patientRess <- rep(1, nPatients);
bracketStart <- seq(1, nPatients, by=100);

for(s in bracketStart){
    cat(s, "\n");
    patientRess[s:(s+99)] <- sapply(s:(s+99), findDisease);
}

## print results
cat(sprintf("%d\n", patientRess), sep="", file=outFileName, append=FALSE);


## p3 - guess manually based on observing the tree
patient.tbl %>%
  mutate(disease = ifelse(height <= 49993, 8941, 5563)) %>%
  group_by(patient, disease) %>%
  summarise(sumContent = sum(nodeContent)) %>%
  group_by(patient) %>%
  slice_max(sumContent, n=1) %>%
  arrange(patient) -> patient.disease.tbl;

cat(sprintf("%d\n", patient.disease.tbl$disease),
    file=outFileName, append=FALSE);

## Note: this doesn't account for branched diseases that match
## branched symptoms

## p4 - guess based on content for exact node matches

patient.tbl %>%
  select(patient, vert, nodeContent) %>%
  left_join(disease.unique.tbl %>%
              select(disease, vert), by="vert") %>%
  arrange(patient, nodeContent) %>%
  filter(!is.na(disease)) %>% ## exclude indirect matches
  group_by(patient, disease) %>%
  summarise(contentSum = sum(nodeContent)) %>%
  group_by(patient) %>%
  slice_max(contentSum, n=1, with.ties=FALSE) %>%
  slice_head(n=1) -> patient.classified.tbl

cat(sprintf("%d\n", patient.classified.tbl$disease),
    file=outFileName, append=FALSE);

## p5 - choose exact node match where specified, otherwise 1240

patient.tbl %>%
  select(patient, vert) %>%
  left_join(disease.unique.tbl %>%
              select(disease, vert), by="vert") %>%
  arrange(patient) %>%
  mutate(disease = replace_na(disease, 1240)) -> patient.classified.tbl;

cat(sprintf("%d\n", patient.classified.tbl$disease),
    file=outFileName, append=FALSE);

## p6 - guess based on content for fully-expanded patient/disease node matches

patient.tbl %>%
  select(patient, vert, nodeContent) %>%
  left_join(disease.tbl %>%
              select(disease, vert), by="vert") %>%
  filter(!is.na(disease)) %>%
  group_by(patient, disease) %>%
  summarise(contentSum = sum(nodeContent)) %>%
  group_by(patient) %>%
  slice_max(contentSum, n=1, with.ties=FALSE) %>%
  slice_head(n=1) -> patient.classified.tbl

cat(sprintf("%d\n", patient.classified.tbl$disease),
    file=outFileName, append=FALSE);

## p7 - guess based on content for fully-expanded patient/disease node matches
## (but split patient table up)

bracketStart <- seq(1, 100000, by=10000);

patient.tbl %>%
    filter(patient >= bracketStart[1], patient < (bracketStart[1] + 10000 - 1)) %>%
    select(patient, vert, nodeContent) %>%
  left_join(disease.tbl %>%
              select(disease, vert), by="vert") %>%
  filter(!is.na(disease)) %>%
  group_by(patient, disease) %>%
  summarise(contentSum = sum(nodeContent)) %>%
  group_by(patient) %>%
  slice_max(contentSum, n=1, with.ties=FALSE) %>%
  slice_head(n=1) -> patient.classified.tbl


cat(sprintf("%d\n", patient.classified.tbl$disease),
    file=outFileName, append=FALSE);

#### full disease matrix generation [p1-p2]


## fill in parent scores
iteration <- 0;
which(diseaseMat > 0, arr.ind = TRUE) %>%
  as_tibble() %>%
  inner_join(parents, by=c("row" = "vert")) %>%
  filter(diseaseMat[cbind(parent, col)] == 0) -> parent.nodes
while(nrow(parent.nodes) > 0){
  iteration <- iteration + 1;
  cat(sprintf("Parent fill iteration: %d; %d rows to fill\n", iteration, nrow(parent.nodes)));
  diseaseMat[parent.nodes[,c("parent", "col")] %>% as.matrix()] <-
    content[parent.nodes %>% pull(parent)]
  which(diseaseMat > 0, arr.ind = TRUE) %>%
    as_tibble() %>%
    inner_join(parents, by=c("row" = "vert")) %>%
    filter(diseaseMat[cbind(parent, col)] == 0) -> parent.nodes
}

## propagate scores to children
iteration <- 0;
which(diseaseMat > 0, arr.ind = TRUE) %>%
  as_tibble() %>%
  inner_join(parents, by=c("row" = "parent")) %>%
  filter(diseaseMat[cbind(vert, col)] == 0) -> child.nodes
while(nrow(child.nodes) > 0){
  iteration <- iteration + 1;
  cat(sprintf("Child fill iteration: %d; %d rows to fill\n", iteration, nrow(child.nodes)));
  diseaseMat[child.nodes[,c("vert", "col")] %>% as.matrix()] <-
    diseaseMat[child.nodes[,c("row", "col")] %>% as.matrix()]
  which(diseaseMat > 0, arr.ind = TRUE) %>%
    as_tibble() %>%
    inner_join(parents, by=c("row" = "parent")) %>%
    filter(diseaseMat[cbind(vert, col)] == 0) -> child.nodes
}

## score patients
for(pp in seq_along(patientLines)){
  p <- patientLines[[pp]];
  scores <- colSums(diseaseMat[p,,drop=FALSE]);
  cat(sprintf("%d\n", head(which(scores == max(scores)), 1)),
      file=outFileName, append=TRUE);
  print(head(which(scores == max(scores)), 1));
}

#####  p3-p7
## write out random classifications, in the hope of getting some right
## this works best for #3 and #5
cat(sprintf("%d\n", sample(nPatients, replace=TRUE)),
    file=outFileName, append=FALSE);

  
## p3
library(magrittr);

## Create disease tibble structure
disease.tbl <- 
  map_dfr(diseaseLines, function(x){tibble(vert=x)}, .id="disease");
for(dp in seq_along(diseaseLines)){
  d <- diseaseLines[[dp]];
  disease.tbl %<>%
    bind_rows(tibble(disease=dp, vert=d));
}

disease.tbl %<>%
  mutate(nodeContent = content[vert]) %>%
  arrange(nodeContent)

## Create patient tibble structure
patient.tbl <- tibble();
for(pp in seq_along(patientLines)){
  p <- patientLines[[pp]];
  patient.tbl %<>%
    bind_rows(tibble(patient=pp, vert=p));
}

patient.tbl %<>%
  mutate(nodeContent = content[vert]) %>%
  arrange(nodeContent)
